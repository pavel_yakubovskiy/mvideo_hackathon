chars_new = {
'Вес' : 'Вес',
'Гарантия' : 'Гарантия',
'Страна' : 'Страна',
'Самоочистка от накипи' : 'накипи',
'Инд. готовности к работе' : 'готовности',
'Прорезиненная ручка' : 'Прорезиненная',
'Потребляемая мощность' : 'мощность',
'Подошва утюга' : 'Подошва',
'Серия' : 'Серия',
'Шаровое крепление шнура' : 'Шаровое крепление шнура',
'Длина сетевого шнура' : 'Длина шнура',
'Мощность подачи пара' : 'Мощность пара',
'Паровой удар' : 'Паровой удар',
'Вертикальное отпаривание' : 'Вертикальное отпаривание',
'Противокапельная система' : 'Противокапельная',
'Распылитель воды' : 'Распылитель',
'Емкость для воды' : 'Емкость',
'Цвет' : 'Цвет',
'Экорежим' : 'Экорежим',
'Авт.отключ. гориз./вертик.' : 'Автоматический отключение',
'Насадка для деликатных тканей' : 'деликатных насадка',
'Авт. регулировка силы пара' : 'регулировка пара',
'Стержень от накипи' : 'Стержень накипи',
'Объем резервуара для воды' : 'Объем резервуара',
'Максимальное давление' : 'Максимальное давление',
'Длина шланга' : 'Длина шланга',
'Автоотключение' : 'Автоотключение',
'Регулируемая подача пара' : 'Регулируемая подача пара',
'Отсек для сетевого шнура' : 'Отсек сетевого шнура',
'Съемный резервуар д/воды' : 'Съемный резервуар',
'Инд. необходимости очистки от накипи' : 'очистки накипи',
'Инд. необходимости долива воды' : 'долива',
'Индикация уровня воды' : 'Индикация уровня',
'Отключение при перегреве' : 'Отключение перегреве',
'Долив воды в процессе глажки' : 'Долив',
'Фиксация утюга' : 'Фиксация',
'Регулировка подачи пара' : 'Регулировка подачи',
'Индикация включения' : 'Индикация включения',
'Подошва отпаривателя' : 'Подошва',
'Примечание101' : 'Примечание101',
'Примечание102' : 'Примечание102',
'Переключение 220 / 110 В' : ' 220 / 110',
'Чехол для хранения' : 'Чехол  хранения',
'Складная ручка' : 'Складная ручка',
'Время нагрева' : 'Время нагрева',
'Производитель процессора' : 'Производитель',
'Частота процессора' : 'Частота',
'Количество ядер' : 'Количество ядер',
'Базовый цвет' : 'цвет',
'Поддержка стандартов' : 'Поддержка',
'Разрешение фотокамеры' : 'Разрешение фотокамеры',
'Автофокус' : 'Автофокус',
'Встроенная вспышка' : 'вспышка',
'Качество видеосъемки' : 'Качество видеосъемки',
'Разрешение фронтальной камеры' : 'Разрешение фронтальной камеры',
'Поддержка GPS' : 'GPS',
'Высота' : 'Высота',
'Ширина' : 'Ширина',
'Глубина' : 'Глубина',
'Встроенный модуль Bluetooth' : ' Bluetooth',
'Количество SIM карт' : ' SIM',
'Тип SIM карты' : ' SIM',
'Оперативная память (RAM)' : 'Оперативная память (RAM)',
'Встроенная память (ROM)' : 'Встроенная память (ROM)',
'Диагональ/разрешение' : 'Диагональ разрешение',
'Диагональ экрана' : 'Диагональ экрана',
'Технология дисплея' : 'Технология дисплея',
'Количество цветов дисплея' : 'цветов дисплея',
'Порт USB' : 'USB',
'Разъем 3.5 мм для подкл. гарнитуры' : '3.5 мм гарнитуры',
'Проводная гарнитура' : 'Проводная гарнитура',
'Кабель для связи с ПК' : 'ПК',
'Зарядное устройство в комплекте' : 'Зарядное',
'Защитная пленка для экрана' : 'Защитная экрана',
'Материал корпуса' : 'Материал корпуса',
'Модель' : 'Модель',
'Операционная система' : 'Операционная система',
'Поддержка Wi-Fi' : 'Wi-Fi',
'Wi-Fi точка доступа' : 'Wi-Fi доступа',
'Цифровой компас' : 'Цифровой компас',
'Датчик ориентации экрана' : 'ориентации экрана',
'Датчик ускорения (G-sensor)' : 'ускорения (G-sensor)',
'Встроенный FM-тюнер' : 'FM-тюнер',
'Габаритные размеры (В*Ш*Г)' : 'размеры (В*Ш*Г)',
'Время в режиме ожидания' : 'ожидания',
'Время в режиме разговора' : 'разговора',
'Емкость аккумулятора' : 'Емкость аккумулятора',
'Тип аккумулятора' : ' аккумулятора',
'Тип процессора' : 'процессора',
'Встроенный парогенератор' : ' парогенератор',
'Авт. контроль оптим. температуры' : 'контроль  температуры',
'Встроенная подставка' : 'Встроенная подставка',
'Петля для подвешивания' : 'Петля подвешивания',
'Регулировка температуры' : 'температуры',
'Насадка-щетка' : 'Насадка щетка',
'Беспроводное использование' : 'Беспроводное',
'Подставка для хранения' : 'Подставка хранения',
'Тип карты памяти' : 'карты памяти',
'Макс. емкость карты памяти' : 'емкость карты памяти',
'Карта памяти' : 'Карта памяти',
'Режим энергосбережения' : 'энергосбережения',
'Защитная рукавица' : 'Защитная рукавица',
'Индикация отсутствия воды' : 'отсутствия',
'Насадка для стрелок' : 'Насадка стрелок',
'Отключение при отсутствии воды' : 'Отключение отсутствии',
'Телескопическая стойка' : 'Телескопическая стойка',
'Примечание103' : 'Примечание103',
'Нас. д/сбора шерсти дом. животных' : 'шерсти  животных',
'Сенсорная панель управления' : 'Сенсорная панель',
'Тип дисплея' : 'дисплея',
'Функция отпаривания' : 'отпаривания',
'Технология Wi-Fi Direct' : 'Wi-Fi Direct',
'Чехол для переноски' : 'Чехол переноски',
'Поддержка DLNA' : 'DLNA',
'Технология NFC' : 'NFC',
'Управление жестами' : 'жестами',
'Беспроводная зарядка' : 'Беспроводная',
'Воронка для залива воды' : 'Воронка залива',
'Сменная панель' : 'Сменная панель',
'Стабилизатор изображения' : 'Стабилизатор',
'USB Host (OTG) кабель' : 'USB Host (OTG)',
'Поддержка USB Host (OTG)' : 'USB Host (OTG)',
'Кабель для цифр.подкл. (HDMI)' : 'HDMI',
'Авт. сматывание шнура' : 'сматывание шнура',
'Стилусов в комплекте' : 'Стилусов',
'Зажимы для брюк и юбок' : 'Зажимы брюк юбок',
'Обратите внимание' : 'внимание',
'Удачное решение' : 'решение',
'Пользователи оценят' : 'Пользователи',
'Порт HDMI' : 'HDMI',
'Формат экрана' : 'Формат экрана',
'Разрешение экрана' : 'Разрешение экрана',
'Воспроизведение MPEG4' : 'MPEG4',
'Воспроизведение DivX' : 'DivX',
'Воспроизведение MP3' : 'MP3',
'Дистанционное управление' : 'Дистанционное управление',
'Тип батарей пульта ДУ' : 'батарей пульта ДУ',
'Стереотюнер' : 'Стереотюнер',
'Звук' : 'Звук',
'Мощность фронтальных АС' : 'Мощность фронтальных АС',
'Вход HDMI' : 'HDMI',
'Вход RCA аудио/видео' : 'RCA аудио видео',
'Вход RCA компонентный YPbPr ' : 'RCA компонентный YPbPr ',
'Вход 3.5 мм аудио' : '3.5',
'Цифровое шумоподавление' : 'шумоподавление',
'Место размещения' : 'размещения',
'Встроенные часы' : 'часы',
'Sleep-таймер' : 'Sleep таймер',
'Настольная подставка' : 'Настольная подставка',
'Настенное крепление' : 'Настенное крепление',
'Телетекст на русском языке' : 'Телетекст',
'Защита от детей' : 'Защита детей',
'Выход коаксиальный цифровой' : 'коаксиальный',
'Вход miniD-Sub видео' : 'miniD-Sub',
'Разъем SCART' : 'SCART',
'Разъем для наушников 3.5 мм' : '3.5',
'Габаритные размеры (без подставки)' : 'Габаритные размеры',
'Вешалка для одежды' : 'Вешалка',
'Серия Samsung' : 'Samsung',
'Аналоговый ТВ тюнер' : 'Аналоговый ТВ тюнер',
'Воспр. медиа с USB' : 'USB',
'Воспроизведение H.264' : 'H.264',
'Воспроизведение JPEG' : 'JPEG',
'Версия HDMI' : 'HDMI',
'Порт USB 2.0 тип A' : 'USB 2.0 A',
'Выход RCA аудио/видео' : 'RCA аудио видео',
'Функция ионизации пара' : 'ионизации',
'Тип дистанционного управления' : 'дистанционного управления',
'Подключ. к горячей воде (60*С)' : 'горячей',
'Реверсивное вращ. активатора' : 'Реверсивное вращение активатора',
'Ручной залив воды в машину' : 'Ручной залив',
'Ручной слив воды из машины' : 'Ручной слив',
'Слив воды из бака' : 'Слив ',
'Таймер продолжит. работы' : 'Таймер ',
'Тип загрузки' : 'загрузки',
'Тип управления' : 'управления',
'Режимов стирки' : 'Режимов',
'Длина заливного шланга' : 'Длина шланга',
'Длина сливного шланга' : 'Длина шланга',
'Материал бака' : 'Материал',
'Ручка для переноски' : 'Ручка переноски',
'Подключ. к холодной воде' : 'холодной ',
'Максимальная загрузка' : 'загрузка',
'Вход RCA аудио' : 'RCA',
'Вход 3.5 мм аудио/видео' : '3.5',
'Мини-пульт ДУ' : 'Мини-пульт ДУ',
'Тип батарей мини-пульта ДУ' : 'батарей мини-пульта ДУ',
'Держатель для шланга' : 'Держатель шланга',
'Тип стекла' : 'стекла',
'Педальное управление' : 'Педальное',
'Складной корпус' : 'Складной',
'Авт.пальцезащитное устр-во' : 'пальцезащитное',
'Ширина гладильного валика (мм)' : 'гладильного валика',
'Скорость вращения валика (об/мин)' : 'Скорость вращения валика',
'Диаметр валика (мм)' : 'Диаметр',
'Регул. скорости валика' : 'скорости валика',
'Вход RCA видео композитный' : 'RCA композитный',
'Вход S-video' : 'S-video',
'Технология Miracast' : 'Miracast',
'Поддержка 4G LTE' : '4G LTE',
'Важная особенность' : 'особенность',
'Поддержка технологии aptX' : 'aptX',
'Цифровой ТВ тюнер' : 'Цифровой ТВ тюнер',
'Разъем для модуля DVB CAM' : 'DVB CAM',
'Жесткий диск (HDD)' : 'HDD',
'Встроенный динамик' : 'динамик',
'Выход miniD-Sub видео' : 'miniD-Sub',
'LAN разъем (RJ45)' : 'LAN RJ45',
'Класс ноутбука' : 'Класс',
'Двухъядерный процессор' : 'Двухъядерный',
'Кэш-память' : 'Кэш-память',
'Графический контроллер' : 'контроллер',
'Поддержка 10/100 FastEthernet' : '10/100 FastEthernet',
'Работа от аккумулятора' : 'аккумулятора',
'Разъем для микрофона 3.5 мм' : 'микрофона 3.5 мм',
'Разъем карт памяти' : 'карт памяти',
'Блок питания' : 'Блок питания',
'Разъем Kensington Lock' : 'Kensington Lock',
'Макс. оперативная память' : 'оперативная',
'Количество слотов памяти ' : 'слотов',
'Разрешение матрицы' : 'матрицы',
'Встроенный микрофон' : 'микрофон',
'Пылевлагозащитный корпус' : 'Пылевлагозащитный',
'Поддержка WiDi' : 'WiDi',
'Управление голосом' : 'голосом',
'Зарядка от USB порта' : 'USB',
'Запись GPS-координат снимка' : 'GPS',
'Хорошо придумано' : 'придумано',
'Покрытие экрана' : 'Покрытие',
'Жесткий диск (SSD)' : 'SSD',
'Чехол для насадок' : 'насадок',
'Разъем под SD/SDHC' : 'SD SDHC',
'Тип кабеля в комплекте 1' : 'кабеля',
'Интерфейс MHL (HDMI - microUSB)' : 'MHL HDMI  microUSB',
'Прорезиненные ножки' : 'ножки',
'Режим "деликатная стирка"' : 'деликатная стирка',
'Макс. загрузка (синтетика)' : 'загрузка синтетика',
'Макс. загрузка (шерсть)' : 'загрузка шерсть',
'Модель Wi-Fi адаптера' : 'Wi-Fi',
'Привод (ODD)' : 'ODD',
'Поддержка Gigabit LAN' : 'Gigabit LAN',
'Аккумуляторов в комплекте' : 'Аккумуляторов',
'*Инструкция на цифровом носителе' : 'Инструкция',
'Производитель видеокарты' : 'видеокарты',
'Выход RCA аудио' : 'RCA',
'Индикация этапов программы' : 'Индикация',
'Класс энергоэффективности' : 'энергоэффективности',
'Энергопотребление за цикл' : 'Энергопотребление',
'Класс стирки' : 'стирки',
'Класс отжима' : 'отжима',
'Расход воды за цикл' : 'Расход воды',
'Диаметр люка' : 'люка',
'Макс. скорость отжима' : 'скорость отжима',
'Защита от протечек' : 'протечек',
'Уровень шума при стирке' : 'шума стирке',
'Уровень шума при отжиме' : 'шума отжиме',
'''Ручная стирка'' шерсти ' : 'стирка шерсти',
'Режим "детские вещи"' : 'детские вещи',
'Дополнительное полоскание' : 'полоскание',
'Отложенный старт' : 'Отложенный',
'Запись с ТВ на USB устройство' : 'ТВ USB',
'Разъем под SD/MMC' : 'SD/MMC',
'Технология' : 'Технология',
'Fax/Modem разъем (RJ11)' : 'RJ11',
'Разъем Express Card' : 'Express Card',
'Тип памяти' : 'памяти',
'Частота памяти' : 'Частота',
'Способ подключ. привода' : 'подключение привода',
'Выход оптический (Toslink)' : 'Toslink',
'Разъем Lightning' : 'Lightning',
'Датчик отпечатков пальцев' : 'отпечатков пальцев',
'Поддержка ГЛОНАСС' : 'ГЛОНАСС',
'Функция "Пульт ДУ"' : 'Пульт ДУ',
'Возможность встраивания' : 'встраивания',
'Наим. защиты от протечек' : 'защиты протечек',
'Разрых. белья после отжима' : 'Разрыхлитель,
'Режим "спорт"' : 'спорт',
'Режим "шторы"' : 'шторы',
'Режим "занавески"' : 'занавески',
'Цвет дверцы люка' : 'Цвет люка',
'Вход коаксиальный цифровой' : 'коаксиальный',
'Ручная стирка  шелка' : 'стирка  шелка',
'Звуковой сигнал' : 'сигнал',
'Порт USB (сервисный)' : 'USB',
'Разъем EXT miniSCART (RGB)' : 'EXT miniSCART',
'Кейс в комплекте' : 'Кейс',
'Преобразование изображения 2D в 3D' : '2D 3D',
'Смартфон в качестве пульта ДУ' : 'Смартфон ДУ',
'Поддержка 3D' : '3D',
'Поддержка Smart TV' : 'Smart TV',
'Подключение к сети LAN' : 'LAN',
'Количество 3D очков в комплекте' : '3D очков ',
'Тип очков' : 'очков',
'Модель 3D очков' : '3D очков',
'Выход 3.5 мм аудио' : '3.5 мм',
'Режим "стирка рубашек"' : 'стирка рубашек',
'Bluetooth (версия)' : 'Bluetooth',
'Макс. такт. частота' : 'частота',
'Разъем mini HDMI' : 'mini HDMI',
'Разъем для наушн./микрофона 3.5мм' : 'наушники микрофона 3.5мм',
'Режим ''замачивание''' : 'замачивание',
'Выход HDMI' : 'HDMI',
'Вход 3.5 мм компонент.YPbPr ' : 'YPbPr ',
'Вход 3.5 мм (сервисный)' : '3.5',
'Инд. времени до конца программы' : 'конца программы',
'Порт USB 3.0 тип A' : 'USB 3.0',
'Рельефная поверхность барабана' : 'поверхность барабана',
'Керамический нагрев. элемент' : 'Керамический элемент',
'Мотор' : 'Мотор',
'Минипрограмма' : 'Минипрограмма',
'Режим "джинсы"' : 'джинсы',
'Режим "отбеливание"' : 'отбеливание',
'Режим "удаление шерсти дом.животных"' : 'удаление шерсти дом.животных',
'Поддержка Skype' : 'Skype',
'Skype камера' : 'Skype',
'Модель Skype камеры' : 'Skype',
'Био-фаза' : 'Био-фаза',
'Разъем Line-In' : 'Line-In',
'Время зарядки аккумулятора' : 'Время аккумулятора',
'Дополнительный пульт ДУ' : 'пульт ДУ',
'Тип батарей дополн. пульта ДУ' : 'пульта ДУ',
'Крюк-держатель' : 'Крюк-держатель',
'Особенность' : 'Особенность',
'Полноразмерная цифр. клавиатура' : 'клавиатура',
'Разъем Line-Out' : 'Line-Out',
'Режим ''пятновыведение''' : 'пятновыведение',
'Режим "антиаллергия"' : 'антиаллергия',
'Режим "очистка барабана"' : 'очистка барабана',
'Выход S-video' : 'S-video',
'Порт e-SATA / USB 2.0' : 'e-SATA  USB',
'Оптимизация стирки' : 'Оптимизация стирки',
'Разъем mini VGA' : 'mini VGA',
'Подсветка клавиш' : 'Подсветка клавиш',
'Оптическое увеличение' : 'Оптическое увеличение',
'Беспроводное ЗУ' : 'Беспроводное',
'Сенсорный дисплей' : 'Сенсорный',
'Режим "ночной" ' : 'ночной',
'Сушка по таймеру' : 'Сушка таймеру',
'Сушка по влажности(хлопок)' : 'влажности хлопок)',
'Сушка по влажности (синт.)' : 'Сушка синтетика',
'Загрузка при сушке(хлопок)' : 'Загрузка сушке',
'Режим "темные ткани"' : 'темные ткани',
'Встроенный барометр' : 'барометр',
'Стабилизатор напряжения' : 'Стабилизатор',
'Режим "верхняя одежда"' : 'верхняя одежда',
'Встроенный сабвуфер' : 'сабвуфер',
'Мощность низкочастотных АС' : 'низкочастотных АС',
'Вертикальная гладильная доска' : 'Вертикальная',
'ЦАП' : 'ЦАП',
'Режим "пуховые изделия"' : 'пуховые изделия',
'Защитное откл. при скачках напряжения' : 'Защитное отключение',
'Быстрая зарядка' : 'зарядка',
'Разъем под SD/SDHC/SDXC' : 'SD SDHC SDXC',
'Вход оптический (Toslink)' : 'Toslink',
'Фоновая подсветка' : 'подсветка',
'Цифровой ТВ тюнер DVB-T' : 'DVB-T',
'Занесение прогр. в память' : 'Занесение память',
'Режим "нижнее белье"' : 'нижнее белье',
'Режим "цветные ткани"' : 'цветные ткани',
'Режим "одеяла/пледы"' : 'одеяла пледы',
'Примечание 305' : '305',
'Прямой привод' : 'Прямой привод',
'Технология ANT+' : 'ANT+',
'Порт eSATA' : 'eSATA',
'Примечание105' : 'Примечание105',
'Пульт ДУ' : 'Пульт ДУ',
'Режим "стирка в холодной воде"' : 'стирка в холодной воде',
'Дозагруз. белья во время программы' : 'Дозагрузка программы',
'Выбор степени загрязнения белья' : 'загрязнения белья',
'Мышь в комплекте' : 'Мышь',
'Генератор пузырьков' : 'Генератор пузырьков',
'Разъем SD' : 'SD',
'Поворотный экран' : 'Поворотный',
'Емкость доп. аккумулятора' : 'Емкость аккумулятора',
'Отсек для стилуса' : 'стилуса',
'Выход DVI' : 'DVI',
'Количество экранов' : 'экранов',
'Режим "постельное белье"' : 'постельное белье',
'Режим "светлые ткани"' : 'светлые ткани',
'Класс водонепроницаемости' : 'Класс водонепроницаемости',
'Календарь' : 'Календарь',
'Калькулятор' : 'Калькулятор',
'Дата / время' : 'Дата время',
'Секундомер' : 'Секундомер',
'Будильник' : 'Будильник',
'Вибровызов' : 'Вибровызов',
'MP3 в качестве звонка' : 'MP3',
'FM приемник' : 'FM',
'Кнопка SOS' : 'SOS',
'Встроенный интернет браузер' : 'браузер',
'Цвет подсветки дисплея' : 'Цвет дисплея',
'Разъем micro USB' : 'micro USB',
'Зарядное устройство' : 'Зарядное устройство',
'Сушка по влажности(шерсть)' : 'сушка шерсть',
'Mini DisplayPort' : 'Mini DisplayPort',
'Режим "кипячение"' : 'кипячение',
'Режим "удаление запахов"' : 'удаление запахов',
'Воспроизведение H.265' : 'H.265',
'"Мобильная" диагностика' : 'диагностика',
'Поддержка ОС' : 'ОС',
'Режим ''ручная стирка''' : 'ручная стирка',
'Специальная функция' : 'функция',
'Разъем micro HDMI' : 'micro HDMI',
'Режим "обработка паром"' : 'обработка паром',
'Авт. парковка барабана' : 'парковка барабана',
'Двойная камера' : 'камера',
'Порт USB 3.1 тип A' : 'USB',
'Порт USB 3.1 тип C' : 'USB',
'Порт e-SATA / USB 3.0' : 'e-SATA USB',
'Модуль 3G' : '3G',
'Монитор сердечного ритма' : 'Монитор ритма',
'Смена панели экрана' : 'панели',
'Ворсовый фильтр' : 'Ворсовый фильтр',
'Цифровой дисплей' : 'Цифровой дисплей',
'Режим "бережной" сушки' : '"бережной"',
'Режим "ускоренной" сушки' : '"ускоренной"',
'Режим сушки шерсти' : ' шерсти',
'Режим сушки шелка' : ' шелка',
'Режим "холодный обдув"' : ' "холодный обдув"',
'Режим сушки подушек' : ' подушек',
'Реверсивное вращ. барабана' : 'Реверсивное барабана',
'Светящиеся символы дисплея' : 'Светящиеся символы',
'Цвет светящихся символов' : 'Цвет светящихся',
'Инд. заполнения резервуара' : ' резервуара',
'Инд. загрязнения фильтра' : ' загрязнения фильтра',
'Набор для слива в дренаж' : 'слив дренаж',
'Сбор конденсата в резервуар' : 'конденсата в резервуар',
'Отвод конденсата в дренаж' : 'Отвод конденсата дренаж',
'Установка на стир. машину' : ' стиральная машину',
'Автоматическое выключение' : 'Автоматическое выключение',
'Самодиагностика неисправн.' : 'Самодиагностика',
'Цвет корпуса' : 'Цвет',
'Глубина с открытым люком' : 'Глубина открытым люком',
'Загрузка при сушке(синт.)' : 'Загрузка синтетика',
'Загрузка при сушке(шерсть)' : 'Загрузка шерсть',
'Тип кабеля в комплекте 2' : ' кабеля  2',
'Тип кабеля в комплекте 3' : ' кабеля  3',
'DisplayPort' : 'DisplayPort',
'Объем барабана' : 'Объем барабана',
'Охлаждение после сушки' : 'Охлаждение',
'Антисминание после сушки' : 'Антисминание',
'Набор для установки ' : 'Набор установки ',
'Наим. компл. для установки' : 'Наименьший комплект для установки',
'Встроенный теплообменник' : 'Встроенный теплообменник',
'Уровень шума' : 'Уровень шума',
'Турборежим' : 'Турборежим',
'Защита от перелива' : 'Защита перелива',
'Обновление операционной системы' : 'Обновление операционной системы',
'Подача воды "душ"' : 'Подача "душ"',
'Примечание104' : 'Примечание104',
'Внутренняя подсветка барабана' : 'Внутренняя подсветка барабана',
'Энергопотреб. (1400 об/мин)' : 'Энергопотребление 1400',
'Энергопотреб. (800 об/мин)' : 'Энергопотребление 800',
'Класс сушки' : 'Класс сушки',
'Давление пара' : 'Давление пара',
'Отключение без воды' : 'Отключение без воды',
'Автоматическая сушка' : 'Автоматическая сушка',
'Режим "функция сушки"' : ' "функция сушки"',
'Режим "сорочки"' : ' "сорочки"',
'Гироскоп' : 'Гироскоп',
'Цвет ремешка' : 'Цвет ',
'Макс. время работы' : 'время работы',
'Водоустойчивый корпус' : 'Водоустойчивый корпус',
'Размер' : 'Размер',
'Материал ремешка' : 'Материал',
'Ремень на руку' : 'Ремень руку',
'Обхват' : 'Обхват',
'Вибросигнал' : 'Вибросигнал',
'Измерение пульса' : 'пульса',
'Яркость' : 'Яркость',
'Режим "дамское белье"' : '"дамское белье"',
'Размер дисплея (В*Ш)' : 'дисплея',
'Порт microUSB 2.0' : 'microUSB 2.0',
'Клавиатура док-станция' : 'Клавиатура док-станция',
'Режим "антибактерия"' : '"антибактерия"',
'Разъем для карт SD/SDHC/SDXC' : 'Разъем для карт SD/SDHC/SDXC',
'Порт Thunderbolt' : 'Thunderbolt',
'Always On Display' : 'Always On Display',
'Порт USB 2.0 тип C' : 'USB 2.0 тип C',
'Порт USB 3.1' : 'USB 3.1',
'Выдвижная панель для сушки' : 'панель сушки',
'Выбор времени заверш. программы' : 'Выбор времени завершение',
'Подсветка дисплея' : 'Подсветка',
'Режим "освежить" (паровой)' : '"освежить" (паровой)',
'Режим "стирка с паром"' : '"стирка паром"',
'TV-тюнер' : 'TV-тюнер',
'Авт. дозатор моющ. средства' : 'дозатор моющего',
'Емк. дозатора жидкого моющего ср-ва' : 'Емкость жидкого моющего',
'Емк. дозатора ополаскивателя' : 'Емкость ополаскивателя',
'Порт USB-C' : 'USB-C',
'Стилус' : 'Стилус',
'Механизм мягкого откр. дверцы' : 'мягкого открывания дверцы',
'Разьем смарт-карт' : 'смарт-карт',
'Поддержка Apple Pay' : 'Apple Pay',
'Разъем сабвуфера' : 'сабвуфера',
'Внешний сабвуфер' : 'Внешний сабвуфер',
'Шлем виртуальной реальности' : 'Шлем виртуальной реальности',
'Авт. регулир. уровня воды' : 'регулирование уровня',
'Предварительная стирка' : 'Предварительная стирка',
'Минипрограмма (30 мин)' : 'Минипрограмма (30 мин)',
'Ускоренная стирка' : 'Ускоренная',
'Модернизация (Upgrade)' : 'Модернизация (Upgrade)',
'Загрузка при тесте' : 'Загрузка тесте',
'Время прог. тестирования' : 'Время тестирования',
'Открывание дверцы на 180*' : 'Открывание дверцы 180*',
'Текстовый дисплей' : 'Текстовый дисплей',
'Экранное меню' : 'Экранное меню',
'Регул. громкости звук. сигнала' : 'Регулирование громкости звукового сигнала',
'Отключение звук. сигнала' : 'Отключение звукового сигнала',
'Инд. времени программы' : 'Индикация программы',
'Направление открыв. люка' : 'Направление открыв.',
'Термоизолир. стекло люка' : 'Термоизолированный стекло',
'Контроль дисбаланса' : 'дисбаланса',
'Контроль пенообразования' : 'пенообразования',
'Размер гладильной поверхн.' : 'гладильной поверхность',
'Автоматическое отключение' : 'Автоматическое отключение',
'Режим "интенсивная сушка"' : 'Режим "интенсивная сушка"',
'Порт USB 3.0 тип C' : 'USB 3.0 тип C',
'Датчик силы нажатия' : 'Датчик силы нажатия',
'Масштабирование до 4K Ultra HD' : 'Масштабирование 4K Ultra HD',
'Телевизор с изогнутым экраном' : 'Телевизор изогнутым',
'Режим "пропитка"' : '"пропитка"',
'mini DisplayPort / Thunderbolt' : 'mini DisplayPort / Thunderbolt',
'Перенавешиваемые двери' : 'Перенавешиваемые',
'Модульный корпус' : 'Модульный',
'Сотовый барабан' : 'Сотовый барабан',
'Емкость аккумулятора док-станции (мАч)' : 'Емкость аккумулятора ',
'Срок гарантии' : 'гарантии',
'Порт FireWire800' : 'Порт FireWire800',
'3D очки' : '3D очки',
'Материал верхней крышки' : 'Материал верхней крышки',
'Инд. отсутствия воды' : 'Индикация отсутствия',
'Фильтр для воды' : 'Фильтр',
'Подставка для утюга' : 'Подставка утюга',
'Вешалка для сушки' : 'Вешалка сушки',
'Режим "разглаживание паром"' : '"разглаживание паром"',
'Экон. времени или электроэнергии ' : 'Экономия времени электроэнергии ',
'Ночной режим' : 'Ночной',
'Режим "смешанные ткани"' : '"смешанные ткани"',
'Технология HDR' : 'HDR',
'Порт microUSB 3.0' : 'microUSB 3.0',
'Режим "самоочистка контура сушки"' : '"самоочистка контура сушки"',
'Режим "распушивание шерсти"' : '"распушивание шерсти"',
'Режим "легкая глажка"' : '"легкая глажка"',
'Дополнительная сушка' : 'Дополнительная сушка',
'Внутреннее освещение' : 'Внутреннее освещение',
'Порт Thunderbolt 2' : 'Thunderbolt 2',
'Количество ядер процессора' : 'ядер процессора',
'Самоочистка теплообменника' : 'Самоочистка теплообменника',
'Подставка для сушки' : 'Подставка сушки',
'Дозатор для жидк. моющего средства' : 'Дозатор жидкого моющего',
'Режим воздушной ионизации' : 'воздушной ионизации',
'Автоматич. выбор режима стирки' : 'выбор стирки',
'Дозатор для жидк. дезинф. средства' : 'Дозатор жидкого дезинфекционного',
'Ящик для ионизации вещей' : 'Ящик ионизации вещей',
'Подсветка барабана' : 'Подсветка барабана',
'Режим "стерилизация"' : '"стерилизация"',
'Цифровое увеличение' : 'Цифровое увеличение',
'Адаптер Lightning - 3.5 мм' : 'Lightning - 3.5 мм',
'Режим "микроволокно"' : '"микроволокно"',
'Сканер радужной оболочки глаза' : 'Сканер',
'Инкрустация кристаллами' : 'Инкрустация кристаллами',
'Широкоугольный объектив' : 'Широкоугольный объектив',
'Телеобъектив' : 'Телеобъектив',
'Тепловой насос' : 'Тепловой насос',
'Конфигурация' : 'Конфигурация',
'Режим "стирка подушек"' : '"стирка подушек"',
'Освещение барабана' : 'Освещение барабана',
'Технология OLED' : 'OLED',
'Порт USB Type-С' : 'USB Type-С',
'Регулировка по высоте' : 'Регулировка высоте',
'Поддув/отвод пара' : 'Поддув отвод',
'Габариты в слож. состоянии' : 'Габариты',
'Порт USB 3.1 тип C / Thunderbolt 3' : 'USB 3.1 C / Thunderbolt 3',
'Режим "пропитывание"' : '"пропитывание"',
'Разъем External Graphics Port' : 'External Graphics',
'Частота обновления' : 'Частота обновления',
'Разъем Thunderbolt 3' : 'Thunderbolt 3',
'Проводные наушники' : 'Проводные',
'Мультифункциональная док-станция' : 'Мультифункциональная',
'Год выпуска' : 'Год',
'Технология G-Sync' : 'G-Sync',
'Док-станция' : 'Док-станция',
'Максимальная вместимость' : 'вместимость',
'Вешалка для рубашек' : 'рубашек',
'Вешалка для брюк' : 'брюк',
'Вешалка для одеял' : 'одеял',
'Пресс для брюк' : 'брюк',
'Лоток' : 'Лоток',
'Кол-во режимов' : 'Количество режимов',
'Управление через смартфон' : 'Управление',
'Вибрация' : 'Вибрация',
'Режим "освежение"' : '"освежение"',
'Создание стрелок на брюках' : 'Создание стрелок',
'Деликатный режим' : 'Деликатный',
'Индикация режима работы' : 'Индикация',
'Панель Touch Bar' : 'Touch Bar',
'Технология квантовых точек' : 'квантовых точек',
'Загрузка верхнего барабана' : 'загрузка',
'Скорость отжима верхнего барабана' : 'Скорость отжима',
'Количество программ у вехрнего барабана' : 'Количество программ',
'Класс энергоэффективности (верхний/нижний барабан)' : 'энергоэффективности',
'Энергопотребление за цикл (верхний/нижний барабан)' : 'Энергопотребление',
'Класс стирки (верхний/нижний барабан)' : 'стирки',
'Класс отжима (верхний/нижний барабан)' : 'отжима',
'Расход воды за цикл (верхний/нижний барабан)' : 'Расход',
'Загрузка нижнего барабана' : 'Загрузка',
'Скорость отжима нижнего барабана' : 'Скорость',
'Количество программ у нижнего барабана' : 'Количество программ',
'Верхний барабан' : 'Верхний барабан',
'Нижний барабан' : 'Нижний барабан',
'Функция отслеживания движений глаз' : 'отслеживания глаз',
'Объем нижнего барабана' : 'нижнего барабана',
'Объем верхнего барабана' : 'верхнего барабана',
'Функция TimeShift' : 'TimeShift',
'Саундбар' : 'Саундбар',
}
