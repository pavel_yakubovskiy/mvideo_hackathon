import requests
import json

API_KEY = '7cb4674161c18fdc3a7af983ab8c7bd03d4c0768'

def get_nouns(input, variant):
    data = [{'text':input}]
    req = requests.Request('POST','http://api.ispras.ru/texterra/v3.1/nlp?targetType=pos-token&apikey={}'.format(API_KEY),
                       headers={'Accept':'application/json', 'Content-Type':'application/json'},data=json.dumps(data))
    prepared = req.prepare()
    
    s = requests.Session()
    resp = s.send(prepared)
    resp = json.loads(resp.text)[0]['annotations']['pos-token']
    
    indicies_to_delete = []
    for i in range(len(resp)):
        if variant == 1:
        # удалить все, кроме S
            if resp[i]['value']['tag'] not in ['S', 'PUNCT']:
                indicies_to_delete.append((resp[i]['start'], resp[i]['end']))
        else:
        # удалить все, кроме S
            if resp[i]['value']['tag'] in ['V', 'A', 'PR']:
                indicies_to_delete.append((resp[i]['start'], resp[i]['end']))
            
    res = ''
    for i in range(len(input)):
        delete_letter = False
        for j in indicies_to_delete:
            if (i >= j[0]) and (i <= j[1]):
                delete_letter = True
        if not delete_letter:
            res += input[i]
    return res
    
        