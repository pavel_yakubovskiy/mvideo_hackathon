import math
import numpy as np
import pandas as pd
from tqdm import tqdm_notebook
from .utils.stop_words import stop_words
from .utils.sentence_processor import SentenceProcessor
#from get_nouns import get_nouns
from .chars_rename.chars import chars_new
import warnings
warnings.filterwarnings('ignore')

# ссылка на модель gensim w2v:
# http://panchenko.me/data/dsl-backup/w2v-ru/tenth.norm-sz500-w7-cb0-it5-min5.w2v
# MODEL_PATH = 'w2v_models/tenth.norm-sz500-w7-cb0-it5-min5.w2v' 
MODEL_PATH = 'w2v_models/all.norm-sz100-w10-cb0-it1-min100.w2v'
sp = SentenceProcessor(tokenizer_regexp='[а-яА-Яa-zA-Z]+')
sp.load_w2v_model(MODEL_PATH)

with open('stop_words/stop_words.txt', 'r', encoding='utf-8') as f:
    s = f.read()
    stop_words = s.split(',')
    stop_words[-1] = stop_words[-1][:-1]

sp.stop_list = stop_words
sp.sample_len = None # максимальная длина комментария в словах

reviews = pd.read_csv('raw_data/dataset1.csv')
charact = pd.read_csv('raw_data/dataset2.csv')

# извлекаем характеристики
product_dict = {}
for name, group in charact.groupby(by='PRODUCT'):
    if name not in product_dict:
        product_dict[name]={}

    product_dict[name]['char']=list(group['NAME'])


def get_chars(product, product_dict):
    """
    1) возвращает лист характеристик товара в токенизированном виде
    2) возвращает лист характеристик в векторном виде
    """
    # TODO отфильтровать стоп слова(предлоги, союзы, междометия и т.д.)
    char_w = []
    char_v = []
    
    for char in product_dict[product]['char']:
        char_new = chars_new.get(char, char)
        char_new = re.sub('[a-zA-Z]+', '', char_new)
        bag_of_words = sp.process(char_new, correction=False)
        #bag_of_words = sp.process(char, correction=False)
        vec = sp.convert2matrix(bag_of_words)
        char_w.append(bag_of_words)
        char_v.append(vec)
        
    return char_v, char_w

def count_word_in_comment(char_v, char_w, comment):
    """
    Принимает:
        слово в виде вектора
        слово в виде токена
        комментарий о товаре в виде pandas.Series (поля TEXT, BENEFITS, DRAWBACKS)
        
    Возвращает:
        количество вхождений слова в ОБЩИЙ комментарий
        количество вхождений слова в ПОЗИТИВНЫЙ комментарий
        количество вхождений слова в НЕГАТИВНЫЙ комментарий
        
    """
    
    CORRECTION = False
    
    q = 0
    q_p = 0
    q_n = 0
    
    # проверяем близость слова (находи синонимы)
    THRESHOLD = 0.6
    
    neutral, positive, negative = None, None, None
    
    if (type(comment.TEXT) != np.float):
        neutral = sp.process(comment.TEXT, correction=CORRECTION)
    
    if (type(comment.BENEFITS) != np.float):
        positive = sp.process(comment.BENEFITS, correction=CORRECTION)
    
    if (type(comment.DRAWBACKS) != np.float):
         negative = sp.process(comment.DRAWBACKS, correction=CORRECTION)
    
    
    # работаем с вектором
    if char_v is not None:
              
        if neutral:
            #print (neutral)
            for word in neutral:
                word = sp.convert2matrix(word, skip=False)
                if word[0] is not None:
                    if char_v.dot(word[0]) > THRESHOLD:
                        q += char_v.dot(word[0])
#                     if np.linalg.norm(char_v - word) < THRESHOLD:
#                         q += 1
            
        if positive:
            for word in positive:
                word = sp.convert2matrix(word, skip=False)
                if word[0] is not None:
                    if char_v.dot(word[0]) > THRESHOLD:
                        q_p += char_v.dot(word[0])
#                     if np.linalg.norm(char_v - word) < THRESHOLD:
#                         q_p += 1
        
        if negative:
            for word in negative:
                word = sp.convert2matrix(word, skip=False)
                if word[0] is not None:
                    if char_v.dot(word[0]) > THRESHOLD:
                        q_n += char_v.dot(word[0])
#                     if np.linalg.norm(char_v - word) < THRESHOLD:
#                         q_n += 1

    # работаем со словом если вектор не удалось найти
    else:
        
        q = neutral.count(char_w)
        q_p = positive.count(char_w)
        q_n = negative.count(char_w)
        
    return q, q_p, q_n


def get_imp_and_sent(char_v, char_w, reviews):
    """
    Принимает:
        характеристика-слово в виде вектора
        характеристика-слово в виде токена
        комментарии, относящиеся к данному товару(из которого взята характеристика)
    
    Возвращает:
        важность характеристики для данного товара (float)
        окраску характеристики для данного товара (float)
    
    """
    importance = 0
    sentiment = 0
    
    for comment in reviews.itertuples():
        
        try:
            rec = int(comment.RECOMMENDED)
        except ValueError:
            rec = 0
            
        try:
            likes = int(comment.LIKES_COUNT)
        except ValueError:
            likes = 0
            
        try:
            dislikes = int(comment.DISLIKES_COUNT)
        except ValueError:
            dislikes = 0
            
        comment_importance = max(
            (likes + rec - dislikes + 1), 
            0
        )
        
        q, q_p, q_m = count_word_in_comment(char_v, char_w, comment)
        importance += (q + q_p + q_m) * comment_importance
        
        ########################
        if (comment.RATING <= 3):
            q = -q
            
        sentiment += (q + q_p - q_m) * comment_importance
        
    return importance, sentiment

def get_reviews(reviews, product_id):
    """Отбирает отзывы относящиеся к данному товару"""
    return reviews[reviews.PRODUCT == product_id]


def get_imp_and_sent_for_product(product_id, product_dict, reviews):
    """
    Принимает:
        id товара (int)
        словарь {товар: [его характеристики]}
        датасет всех отзывов (pd.DataFrame)
        
    Отдает:
        лист важности (по каждой характеристике)
        лист тональности (по каждой характеристике)
        
    """
    
    char_v, char_w =  get_chars(product_id, product_dict)
    
    char_importance = []
    char_sentiment = []
    
    reviews = get_reviews(reviews, product_id)
    
    # итерируем по каждой характеристике
    for i in range(len(char_v)):
        
        char_importance.append([])
        char_sentiment.append([])
        
        # итерируем по каждому слову в харакеритике 
        for j in range(len(char_v[i])):
            importance, sentiment = get_imp_and_sent(char_v[i][j], char_w[i][j], reviews)
            char_importance[i].append(importance)
            char_sentiment[i].append(sentiment)
            
        if char_importance[i] == []:
            char_importance[i].append(0)
        if char_sentiment[i] == []:
            char_sentiment[i].append(0)    
            
    product_dict[product_id]['char_importance'] = char_importance
    product_dict[product_id]['char_sentiment'] = char_sentiment
    
    return char_importance, char_sentiment


def get_value_of_char(product_id, category_name, df):
    a = df[df.PRODUCT == product_id]
    a = a[a.NAME == category_name]
    value = a.VALUE.values
    
    if type(a.MEASURE.values[0]) == float:
        measure = ''
    else:
        measure = ' ' + a.MEASURE.values[0]
    return str(value[0]) + str(measure)


def get_top_k(product_id, K):#for product_id in pd.Series(list(set(reviews.PRODUCT).intersection(set(charact.PRODUCT)))).values:
    product_id = product_id
    char_importance, char_sentiment = get_imp_and_sent_for_product(product_id, product_dict, reviews)

    char_importance_mean = list(map(lambda x: np.mean(x), char_importance))
    char_sentiment_mean = list(map(lambda x: np.mean(x), char_sentiment))

    product_df = charact[charact.PRODUCT == product_id]
    product_df['IMPORTANCE'] = char_importance_mean
    product_df['SENTIMENT'] = char_sentiment_mean
    
    top_k = []
    for i in range(K):
        name = product_df.sort_values('IMPORTANCE')[::-1]['NAME'].iloc[i]
        value = product_df.sort_values('IMPORTANCE')[::-1]['VALUE'].iloc[i]
        imp = product_df.sort_values('IMPORTANCE')[::-1]['IMPORTANCE'].iloc[i]
        sent = product_df.sort_values('IMPORTANCE')[::-1]['SENTIMENT'].iloc[i]
        print ('{}: {}\n\t\tImportance {:.2f}\t\tSentiment {:.2f}'.format(name, value, imp, sent))
        top_k.append([name, value, imp, sent])

    return top_k
